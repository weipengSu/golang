package learning_15

import (
	"fmt"
	"runtime"
	"testing"
	"time"
)

func RunTask(id int) string  {
	time.Sleep(10 *time.Millisecond)
	return fmt.Sprintf("The result is from %d", id)
}

func FirstResponse() string  {
	numRunner := 10
	// 需要使用buffer chan,要考虑协程泄露
	ch := make(chan string, numRunner)
	for i:=0; i< numRunner; i ++{
		go func(i int) {
			ret := RunTask(i)
			ch <- ret
		}(i)
	}

	return <- ch
}

func TestFirstResponse(t *testing.T)  {
	// 输出当前系统的协程数
	t.Log(runtime.NumGoroutine())
	t.Log(FirstResponse())
	// 输出当前系统的协程数
	t.Log(runtime.NumGoroutine())
}