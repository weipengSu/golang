package learning_15

import (
	"fmt"
	"runtime"
	"testing"
	"time"
)

func RunTasks(id int) string  {
	time.Sleep(10 *time.Millisecond)
	return fmt.Sprintf("The result is from %d", id)
}

func AllResponse() string  {
	numRunner := 10
	// 需要使用buffer chan,要考虑协程泄露
	ch := make(chan string, numRunner)
	for i:=0; i< numRunner; i ++{
		go func(i int) {
			ret := RunTasks(i)
			ch <- ret
		}(i)
	}

	finalret := ""
	for j := 0; j < numRunner; j ++{
		finalret += <- ch + "\n"
	}

	return finalret
}

func TestAllResponse(t *testing.T)  {
	// 输出当前系统的协程数
	t.Log(runtime.NumGoroutine())
	t.Log(AllResponse())
	// 输出当前系统的协程数
	t.Log(runtime.NumGoroutine())
}