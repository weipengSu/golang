package learning_15

import (
	"fmt"
	"sync"
	"testing"
	"unsafe"
)


// 使用once 只运行一次
type Singleton struct {

}

var singleInstance *Singleton

var once sync.Once

func GetSingletonObj() *Singleton {
	once.Do(func() {
		fmt.Println("Craete Obj")
		singleInstance = new(Singleton)
	})
	return singleInstance
}

func TestGetSingletonObj(t *testing.T)  {
	var wg sync.WaitGroup
	for i := 0; i < 10; i ++{
		wg.Add(1)
		go func() {
			obj := GetSingletonObj()
			fmt.Printf("%d\n", unsafe.Pointer(obj))
			wg.Done()
		}()
	}
	wg.Wait()
}