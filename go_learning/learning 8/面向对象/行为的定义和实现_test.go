package 面向对象

import (
	"fmt"
	"reflect"
	"testing"
	"unsafe"
)

// 封装数据和行为

// 封装数据类型
type Employee struct {
	Id   string
	Name string
	Age  int
}

// 实例创建与初始化
func TestInstance(t *testing.T)  {
	// 初始化1
	e := Employee{"0", "1", 2}
	// 初始化2
	e1 := Employee{
		Id:   "0",
		Name: "1",
		Age:  2,
	}

	// 初始化三
	e2 := new(Employee)  // 实例化的是指针
	e2.Id, e2.Name, e2.Age = "0", "1", 2

	t.Log(e, e1, *e2)
	t.Log(reflect.TypeOf(e), reflect.TypeOf(e1), reflect.TypeOf(*e2))

}


// 行为定义

// 第一种定义方式在实例对应方法被调用时，实例的成员会进行复制, 会有一个内存复制的开销
func (e Employee) String() string  {
	fmt.Printf("Address is %x\n", unsafe.Pointer(&e.Name))
	return  fmt.Sprintf("ID:%s-Name:%s-Age:%d", e.Id, e.Name, e.Age)
}

// 通常情况下为了避免内存拷贝，我们使用第二种定义方式
// 使用此方法没有内存复制产生，所有的实例方法都存放在同一位置
// 建议采用此种方式
func (e *Employee) String1() string  {
	fmt.Printf("Address is %x\n", unsafe.Pointer(&e.Name))
	return  fmt.Sprintf("ID:%s-Name:%s-Age:%d", e.Id, e.Name, e.Age)
}

func TestCreateEmployeeObj(t *testing.T)  {
	e := &Employee{"0", "1", 2}
	t.Logf("Address is %x\n", unsafe.Pointer(&e.Name))
	t.Log(e.String())
}