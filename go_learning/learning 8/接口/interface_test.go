package 接口

import (
	"fmt"
	"testing"
)

// 接口定义
type Programmer interface {
	WriteHelloWorld() string
}


// 接口实现
// 定义实例
type GoProgrammer struct {

}


type Testprogrammer struct {

}


// 定义函数
func (p *GoProgrammer) WriteHelloWorld() string  {
	return fmt.Sprint("Hello World")
}

func (p * Testprogrammer) WriteHelloWorld() string  {
	return fmt.Sprint("we can")
}

func TestClient(t *testing.T)  {
	// 可以理解为层层注入
	var p Programmer
	p = new(Testprogrammer)
	t.Log(p.WriteHelloWorld())
	//p1 := new(Testprogrammer)
	//t.Log(p1.WriteHelloWorld())
}



