package 接口

import (
	"fmt"
	"testing"
)

type Read interface {
	read ()
}

type Write interface {
	write()
}


type ReadWrite interface {
	Read
	Write
}


type Data struct {
	
}

func (d *Data) read()  {
	fmt.Println("获得的值为Read")
}

func (d *Data) write()  {
	fmt.Println("获得的值为Write")
}

func TestReadWrite(t *testing.T)  {
	var rw ReadWrite
	rw = new(Data)
	rw.read()
	//fmt.Printf("%T", s)
	//t.Log()
}