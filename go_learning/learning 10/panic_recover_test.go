package learning_10

import (
	"errors"
	"fmt"
	"testing"
)

// panic
// panic 用于不可恢复的错误
// panic 退出前会执行defer 指定的内容

// panic vs os.Exit
// os.Exit 退出时不会调用defer 指定的函数
// os.Exit 退出时不输出当前调用栈信息

func TestPanicVxExit(t *testing.T)  {
	defer func() {
		fmt.Println("嘿嘿")
	}()
	t.Log("defer 退出")
	panic(errors.New("hello world"))
	//os.Exit(-1)
}


// recover 代表捕捉所有错误
// defer fun(){
//	if err := recover(); err != nil{
//		// 恢复错误
//  }
// }()

func TestRecover(t *testing.T)  {
	defer func() {
		if err := recover(); err != nil{
			fmt.Println("recoved from", err)
		}
	}()

	fmt.Println("start")
	panic(errors.New("something wrong"))
}

// 千万不要这样干， 如果不处理错误，直接recover 的话 当心 形成僵尸进程, 导致health check 失效
// let it crash ! 往往是我们恢复不确定性错误的最好方法