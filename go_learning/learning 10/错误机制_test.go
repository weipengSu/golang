package learning_10

import (
	"errors"
	"testing"
)

// Go的错误机制
// 1: 没有异常机制
// 2: error 类型实现了error 接口
// 3: 可以通过errors.New 来快速创建错误实例
// go 语言逻辑快速失败, 及早失败，避免嵌套


func GetFinbonacci(n int) ([]int, error)  {
	fibList := []int{1, 1}
	if n < 0 || n > 1000{
		return nil, errors.New("传入的值不能小于 0")
	}
	for i:= 2; i < n; i++{
		fibList = append(fibList, fibList[i-2] + fibList[i-1])
	}

	return fibList, nil
}

func TestGetFinbonacci(t *testing.T)  {
	if v, err := GetFinbonacci(-10); err == nil{
		t.Log("输出v:", v)
	}else{
		t.Log("程序报错", err)
	}
}