package learning_5

import (
	"fmt"
	"testing"
)

// map 声明的三种方式
// 声明方式1   m := map[string]int{"one": 1, "two": 2, "three": 3}
// 声明方式2   m1 := map[string]int{} m1["one"] = 1
// 声明方式3   m2 := make(map[string]int, 10)

func TestMap(t *testing.T)  {
	m := map[string]int{
		"one": 1,
		"two": 2,
		"three": 3,
	}

	m1 := map[string]int{}
	m1["one"] = 1


	m2 := make(map[string]int, 10)
	m2["hello"] = 2

	fmt.Println(m , m1 ,m2)
}

func TestNotExistKey(t * testing.T)  {
	a := map[int]int{}
	a[2] = 1
	t.Log(a)
	if v, ok := a[3]; ok{
		t.Log("他是存在的", v)
	}else{
		t.Log("is not exist", nil)
	}
}

func TestMaptravel(t *testing.T)  {
	m1 := map[int]int{1: 1, 2: 2, 3: 3}
	for index, value := range m1{
		t.Log(index, value)
	}
}