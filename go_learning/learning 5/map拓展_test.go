package learning_5

import "testing"

// 函数是一等公民
// map的value 除了是一些值以外, 也可以是一个方法
// 与go 的dock type 接口方式一起，可以方便的实现单一方法对象的工厂模式

func TestMapWithFuncValue(t *testing.T) {
	m := map[int]func(op int) int{}
	m[1] = func(op int) int { return op }
	m[2] = func(op int) int { return op * op }
	m[3] = func(op int) int { return op * op * op }
	t.Log(m[1](2), m[2](2), m[3](2))
}

func TestDeleteMap(t *testing.T)  {
	m1 := map[int]int{
		1: 1,
		2: 2,
		3: 3,
	}

	t.Log(m1)

	delete(m1, 2)
	t.Log(m1)
}
// 实现set
// Go的内置集合中没有set 实现，可以map[type]bool
// map 删除函数 可以使用delete
