package learning_14

import (
	"fmt"
	"testing"
	"time"
)

func isCanceller(cancelChan chan  struct{}) bool {
	select {
	case <- cancelChan:
		return true
	default:
		return false
}}

func cancel_1(cancelChan chan struct{})  {
	cancelChan <- struct{}{}
}

func cancel_2(cancelChan chan struct{})  {
	close(cancelChan)
}

func TestCancel(t *testing.T)  {
	cancelChan := make(chan struct{}, 0)
	for i := 0; i < 5; i ++{
		go func(i int, cancel chan struct{}) {
			for {
				if isCanceller(cancelChan){
					break
				}
				time.Sleep(time.Millisecond * 5)
			}
			fmt.Println(i, "cancelled")
		}(i, cancelChan)
	}
	cancel_2(cancelChan)
	time.Sleep(time.Second*1)
}