package learning_14

// 根context: 通过context.Background() 创建
// 子context: context.withCancel(parentContext) 创建
//	ctx, cancel := context.WithCancel(context.Background())
// 当前context被取消的时候，基于他的context 都会被取消
// 接受取消通知 <- ctx.Done()