package learning_7

import "testing"

// 可变长参数 ...int， 会被转换成数组

func SlowSum(opts ... int)  int{
	ret := 0
	for _, value := range opts{
		ret += value
	}

	return ret
}

func TestSum(t *testing.T)  {
	t.Log(SlowSum(1, 2,3,4,5))
	t.Log(SlowSum(1,2,3,4))
}


// defer延迟函数, 可用于最后关闭连接,即使出现panic 也会停止, 除了defer操作，其他操作在panic 后不执行
func TestDefer(t *testing.T)  {
	defer func() {
		t.Log("clear resources")
	}()

	t.Log("start")
	panic("found problem")
}