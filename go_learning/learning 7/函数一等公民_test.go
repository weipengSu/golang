package learning_7

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

// 与其他编程语言的差异
// 1: 可以有多个返回值
// 2: 所有参数都是值传递，slice, map, channel 都会有传引用的错觉
// 3: 函数可以作为变量的值
// 4: 函数可以作为参数和返回值

func ReturnMultiValues()  (int, int){
	return rand.Intn(10), rand.Intn(20)
}

func TimeSplit(inner func(op int) int) func(op int) int {
	return func(op int) int {
		start := time.Now()
		ret := inner(op)
		fmt.Println("time spend:", time.Since(start).Seconds())
		return ret
	}
}

func SlowFun(op int) int {
	time.Sleep(time.Second*1)
	return op
}

func TestFn(t *testing.T)  {
	a, b := ReturnMultiValues()
	t.Log(a, b)
	tsF := TimeSplit(SlowFun)
	t.Log(tsF(10))
}