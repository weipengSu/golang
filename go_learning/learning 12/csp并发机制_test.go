package learning_12

import (
	"fmt"
	"testing"
	"time"
)

// csp 模式是通过channel 进行通讯的，更松耦合一些
// go中channel 是有容量的并且独立处理groutine, 而如erlang, actor模式中的mailbox 容量是无限的，接受进程也总是被动的处理消息。

func service() string  {
	time.Sleep(time.Millisecond * 50)
	return "Done"
}

// channel 无容量
func AsyncService() chan string {
	retCh := make(chan string)
	go func() {
		ret := service()
		fmt.Println("returned result")
		retCh <- ret
		fmt.Println("service exited")
	}()
	return retCh
}


// channel 有容量
func AsyncServiceBuffer() chan string {
	retCh := make(chan string, 2)
	go func() {
		ret := service()
		fmt.Println("returned result")
		retCh <- ret
		fmt.Println("service exited")
	}()
	return retCh
}


func TestAnsyService(t *testing.T)  {
	ret := AsyncService()
	fmt.Println(<- ret)

	ret1 := AsyncServiceBuffer()
	fmt.Println(<- ret1)
}


