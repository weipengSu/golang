package learning_12

import (
	"fmt"
	"testing"
)
// chan buffer 执行完毕之后记得关闭 chan close()
func AsyncChan() chan int {
	ret := make(chan int, 10)
	for i := 0; i < 10; i ++{
		ret <- i
	}
	close(ret)
	return ret
}


// 获取方式一: 清空chan里面的值
func TestChann(t *testing.T)  {
	ret := AsyncChan()
	for {
		if value, ok := <-ret; ok{
			fmt.Println("获得的值为:", value)
		}else{
			fmt.Println("输出完毕")
			break
		}
	}
}

