package learning_12

import (
	"sync"
	"testing"
	"time"
)

// 共享内存并发机制, 资源在临界区相互抢占, 需要枷锁控制
func TestCounter(t *testing.T)  {
	counter := 0
	for i := 0; i < 5000; i ++{
		go func(i int) {
			counter += i
		}(i)
	}
	time.Sleep(1 *time.Second)
	t.Logf("counter == %d", counter)
}


// 枷锁
func TestMutextCounter(t *testing.T)  {
	var mut sync.Mutex
	counter :=0
	for i := 0; i < 5000; i ++{
		go func(i int) {
			defer func() {
				mut.Unlock()
			}()
			mut.Lock()
			counter ++
		}(i)
	}
	time.Sleep(1 * time.Second)
	t.Logf("counter == %d", counter)
}


// WaitGroup
// 三个变量  wg.Add()  wg.Done()  wg.Wait()

func TestMutexWaitGroup(t *testing.T)  {
	var mut sync.Mutex
	var wg sync.WaitGroup
	counter := 0
	for i := 0; i < 5000; i ++{
		wg.Add(1)
		go func() {
			defer func() {
				mut.Unlock()
			}()
			mut.Lock()
			counter ++
			wg.Done()
		}()
	}
	wg.Wait()
	t.Logf("counter == %d", counter)
}