package learning_4

import (
	"testing"
)

// 切片和数组比较， 数组前面必须指定大小，切片不用
// make([]int, len, cap) cap 被称为容量, 就是用来扩容的

//如果切片的容量小于1024个元素，那么扩容的时候slice的cap就翻番，乘以2；一旦元素个数超过1024个元素，增长因子就变成1.25，即每次增加原来容量的四分之一。
//如果扩容之后，还没有触及原数组的容量，那么，切片中的指针指向的位置，就还是原数组，如果扩容之后，超过了原数组的容量，那么，Go就会开辟一块新的内存，把原来的值拷贝过来，这种情况丝毫不会影响到原数组。

func TestSliceInt(t *testing.T) {
	var s0 []int
	t.Log(len(s0), cap(s0))

	s0 = append(s0, 1)
	t.Log(len(s0), cap(s0))

	// 定义声明
	// 切片声明   var s0 []int,   s:= []int[}  s := []int{1,2,3}  s:= make([]int, 2, 4)
	s2 := make([]int, 3, 5)
	for i := 0; i < 10; i++ {
		s2 = append(s2, i)
	}

	t.Log(s2, cap(s2), len(s2))
}

func TestArraySlice(t *testing.T) {
	a0 := []int{}
	a1 := [3]int{}
	t.Logf("%T %T", a0, a1)
	for i := 0; i < 17; i ++{
		a0 = append(a0, i)
	}
	t.Log(a0, len(a0), cap(a0))
}
