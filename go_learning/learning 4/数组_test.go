package learning_4

import (
	"fmt"
	"testing"
)

// 声明数组

func TestArrayInit(t *testing.T)  {
	// 声明数组, 必须指定大小
	var arr [3]int

	// 如果不想去指定长度
	arr1 := [...]int{1, 2, 3, 4}

	t.Log(arr, arr1)
}

func TestArrayTravel(t *testing.T)  {
	arr3 := [...]int{1,2,3,4,5}
	// for循环遍历数组, 在循环的时候, index表示下标, value 表示值,如果不想显示，则可以使用 _
	for index, value := range arr3{
		fmt.Println(index, value)
	}

	// 支持数组截取, 不支持负数切片
	fmt.Println("数组截取:", arr3[1: 3])
}