package main

import (
	"fmt"
	"os"
)

// main 函数不支持函数返回值，要想获得返回状态，可以使用 os.Exit()

// main 函数打印命令行参数, 使用 os.Args, 返回的是一个数组

func main() {
	if len(os.Args) > 1{
		fmt.Println(os.Args[1])
	}
	fmt.Println("hello world")
	os.Exit(-1)
}
