package learning_11

// package
// 基本复用模块    以首字母大写来表明可被包外代码访问
// 代码里的package 可以和所在目录不一致
// 同一目录里的Go代码的package 要保持一致


// init
// 在main 被执行前, 所有以依赖的package的init 方法都会被执行
// 不同包的init函数 按照包导入的依赖关系决定执行顺序
// 每个包可以有多个Init函数
// 包的每个源文件也可以有多个init函数，这点比较特殊