package learning_9

import (
	"fmt"
	"testing"
)

type Pet struct {

}

func (p *Pet) Speak() {
	fmt.Println("...")
}

func (p *Pet) SpeakTo (host string)  {
	p.Speak()
	fmt.Println("", host)
}

func TestDog(t *testing.T)  {
	var p Pet
	p.SpeakTo("嘿嘿")
}