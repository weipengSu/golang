package learning_9

import (
	"fmt"
	"testing"
)

//  interface
//  空接口可以表示任何类型
//	通过断言来将空接口转换为制定类型

func DoSomething(p interface{}){
	//if i, ok := p.(int); ok{
	//	fmt.Println("Integer", i)
	//}
	//
	//if s, ok := p.(string); ok{
	//	fmt.Println("String", s)
	//}
	//
	//fmt.Println("unknow type")
	switch v := p.(type) {
	case int:
		fmt.Println("Integer", v)
	case string:
		fmt.Println("String", v)
	default:
		fmt.Println("unknow type")
	}
}

func TestEmptyInterface(t *testing.T)  {
	DoSomething(10)
	DoSomething("10")
}