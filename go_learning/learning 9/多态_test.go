package learning_9

import (
	"fmt"
	"testing"
)

type Code string

type Programmer interface {
	WriteHelloWorld() Code
}

type GoProgrammer struct {

}

func (p *GoProgrammer) WriteHelloWorld() Code  {
	return "hello world go"
}

type JavaProgrammer struct {

}

func (p *JavaProgrammer) WriteHelloWorld() Code  {
	return "hello world java"
}

// 使用多态
func WriteFirstProgram(p Programmer)  {
	fmt.Printf("%T %v \n", p, p.WriteHelloWorld())
}

func TestPloymorphism(t *testing.T)  {
	goProg := new(GoProgrammer)
	javaProg := new(JavaProgrammer)

	WriteFirstProgram(goProg)
	WriteFirstProgram(javaProg)
}