package learning_13

import (
	"fmt"
	"sync"
	"testing"
)

// 可以通过close 关闭,可设置一个发送者,多个接收者
// 不能往关闭的通道上发消息，向关闭的chann上面发送数据，会导致panic
// v, ok <- ch; ok为bool值， true 表示正常接受,false表示通道关闭
// 所有channel接收者都会在channel 关闭的时候，立即从阻塞等待中返回且上述ok为false，这个广播机制常被利用，进行向多个订阅者同时发送信号

func DataProducer(ch chan int, wg *sync.WaitGroup)  {
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		close(ch)
		wg.Done()
	}()
}

func DataRecevicer(ch chan int, wg *sync.WaitGroup)  {
	go func() {
		for {
			if data, ok := <-ch; ok{
				fmt.Println("获得的值为:", data)
			}else{
				fmt.Println("释放完毕")
				break
			}
		}
		wg.Done()
	}()
}

func TestCloseChannel(t *testing.T)  {
	//var mut sync.Mutex
	var wg sync.WaitGroup
	ch := make(chan int)
	wg.Add(1)
	DataProducer(ch, &wg)
	wg.Add(1)
	DataRecevicer(ch, &wg)
	wg.Add(1)
	DataRecevicer(ch, &wg)
	wg.Wait()
}