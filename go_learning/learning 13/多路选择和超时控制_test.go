package learning_13

import (
	"strconv"
	"testing"
	"time"
)

// 多路渠道 select， 超时控制 time.After

func AsyncService(ret chan string) chan string {
	for i := 0; i < 10; i ++{
		ret <- strconv.Itoa(i)
		time.Sleep(200 *time.Millisecond)
	}

	close(ret)
	return ret
}

func TestAsncService(t *testing.T)  {
	ret := make(chan string, 10)
	data_ret := AsyncService(ret)
	select {
	case ret1 := <- data_ret:
		t.Log(ret1)
	case <- time.After(time.Millisecond * 100):
		t.Error("time out")
		break
	}
}