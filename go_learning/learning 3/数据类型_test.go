package learning_3

import (
	"fmt"
	"reflect"
	"testing"
)

// 类型转化:  1. Go语言不允许隐式类型转换   2. 原类型和别名之间也不能隐式类型转换


func TestImp(t *testing.T)  {
	var a int32 = 1
	var b int64
	c := int64(a)
	fmt.Println(c == b)
	// 不允许使用  t.Log(a == b)
}


// 指针类型  获取指针地址 &a, 获取指针地址的值 *aPtr, 但是不支持指针运算
func TestPoint(t *testing.T)  {
	a := 1
	aPtr := &a
	t.Log("获取指针地址:", aPtr)
	fmt.Println(reflect.TypeOf(aPtr))  // 获取指针类型， 可以使用reflect.TypeOf()
	t.Logf("%T",aPtr)   // 获取指针类型， 可以使用%T, 记得使用函数带上f,例如logf, Printf
	t.Log("获取指针地址的值:", *aPtr)
}

// string 类型 默认类型是nil, 不是null
func TestString(t *testing.T)  {
	var s string
	t.Log("*" + s + "*")
}