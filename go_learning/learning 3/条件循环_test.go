package learning_3

import (
	"testing"
)

// go 里面只支持一个for 循环, 不支持 while

func TestWhileLoop(t *testing.T) {
	n := 0
	/*while n < 5*/
	for n < 5 {
		t.Log(n)
		n++
	}

	/*while true*/
	//	for {
	//		t.Log(n)
	//	}
}

// if 条件, 1: condition 表达式结果必须为布尔值  2: 支持变量赋值
func SomeFun() (int, error) {
	return 10, nil
}

func TestIfMultiSec(t *testing.T) {
	if a, err := SomeFun(); err == nil {
		t.Log("获得正常的值为:", a)
	} else {
		t.Log(" 获取的值错误", err)
	}
}

//  switch 条件
//  1: 不限制为常量或者整数, 举例 swith os := runtime.GOOS; os{}
//  2: 单个case 中, 可以出现多个结果选项，使用逗号分隔;
//  3: 与其他编程语言相比，go语言不需要break来明确推出一个case;
//  4: 可以不设定swtich 之后的条件表达式，在此种情况下, 整个swithch结构与多个if..else.. 的逻辑作用相同

func TestSwitch(t *testing.T) {
	for i := 0; i < 5; i++ {
		switch i {
		case 0,2:
			t.Log("Even")
		case 1, 3: // 对应上面的2
			t.Log("Odd")
		default:
			t.Log("it is not 0 - 3")
		}
	}
}

func TestSwitchCaseCondition(t *testing.T)  {
	for i := 0; i < 5; i ++{
		switch  {
		case i % 2== 0:
			t.Log("Even")
		case i % 2 != 0:
			t.Log("Odd")
		default:
			t.Log("it is not 0 - 3")
		}
	}
}