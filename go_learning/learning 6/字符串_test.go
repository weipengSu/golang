package learning_6

import (
	"reflect"
	"strconv"
	"strings"
	"testing"
)

// 字符串与其他编程语言的差异

// 1: string 是数据类型，不是引用或者指针类型
// 2: string 是只读的byte slice, len 函数可以包含他所包含的byte 数
// 3: string 的byte 数组可以存放任何数据



// split 切割, join 连接
func TestSplit(t *testing.T)  {
	s := "A, B, C"
	parts := strings.Split(s, ",")
	t.Log(parts)

	// join 连接
	t.Log(strings.Join(parts, "-"))

}



// str --> int
func TestStrToInt(t *testing.T)  {
	s := strconv.Itoa(10)
	t.Log(s, reflect.TypeOf(s))
}


// int --> str
func TestIntToStr(t * testing.T)  {
	if s, err := strconv.Atoi("10"); err == nil{
		t.Log(s, reflect.TypeOf(s))
	}else{
		t.Log("转换失败")
	}
}