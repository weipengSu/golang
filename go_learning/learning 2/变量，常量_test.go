package learning_2

import (
	"fmt"
	"testing"
)

// 单元测试, 在命名的时候 需要以 _test.go 结尾, 在定义 func 的时候  需要以 Test开头，带上 t*testing.T

func TestFibonacciCreation(t *testing.T) {
	t.Log("My first try")

	var (
		a int = 1
		b int = 1
		c     = make([]int, 0)
	)

	for i := 1; i < 10; i++ {
		a, b = b, a+b
		c = append(c, a)
	}

	fmt.Println(c)
}

// 常量定义, 可以使用递增 可以使用iota

const (
	Monday = iota + 1
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

const (
	Open = 1 << iota
	Close
	Pending
)

func TestIota(t *testing.T) {
	a := 7
	t.Log(Open, Close, Pending)
	t.Log(Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday)
	t.Log(a&Open, a&Close, a&Pending)
}
